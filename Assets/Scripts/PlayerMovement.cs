using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController characerControl;
    private Animator animator;

    float horizontal;
    float vertical;
    bool isCrouching = false;

    public float moveSpeed = 0.3f;

    void Start()
    {
        characerControl = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        animator.SetFloat("Forward", vertical);
        animator.SetFloat("Right", horizontal);

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isCrouching = true;
            animator.SetBool("isCrouching", isCrouching);
        }else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isCrouching = false;
            animator.SetBool("isCrouching", isCrouching);
        }
    }
}
